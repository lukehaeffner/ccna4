CCNA Chapter 1
==============

Contents
---
1. __Implementing a Network Design__
	* Heirarchical Design Model
	* Failire Domains
	* Network Scalability
	* Redundancy
	* Implementation of Wireless Connectivity
	* Routing Protovols (EIGRP, OSPF)

2. __Selecting Network Devices__
	* Switch Hardware
	* Router Hardware
	* Power Management

 - - - 

Implementing a Network Design
---
###Heirarchical Design Model

Having a heirarchical design helps *evenly distribute* the work load across the whole network as well as *optomizing performance*. 

Example Commands to show how it will look
		
	Switch> enable
	Switch# conf t
	Switch# more example inputs

It is made up of 3 layers

> __1. Core Layer__
		
> > This layer is the high speed backbone layer between the networks

>  __2. Distribution Layer__
		
>> This layer is used to forward traffic from one loval connection to another

> __3. Access Layer__

>> This layer provides connectivity to the users
