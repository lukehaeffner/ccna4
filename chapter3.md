Chapter 3 - Point to Point Connections
==============

Contents
---
1. __Serial Communications__
	* Standards
	* Multiplexing


2. __Selecting Network Devices__
	* Switch Hardware
	* Router Hardware
	* Power Management

 - - - 

Serial Communications
----
One of the most common types of WAN connections are __point-to-point__ connections. These are used to connect LAN's to *service provider* WANs', and also to connect LAN's within an enterprise networks.

### Serial Communication Standards

>__RS-232__: Those old 9/25 pin connectors used for mice and keyboards

>__V.35__: High speed, synchronous data exchange using telephone circuits. Serials using DTE/DCE

>__HSSI__: *HIgh-Speed-Serial-Interface*. Supports transmission rates __up to 52mb/s__. Used to connect routers on LANs with WANS over high speed lines. Scrubby cisco propiertary  DTE/DCE communication

### Multiplexing

Two types of muliplexing are 

>__Time-Division Multiplexing (TDM)__
>> TDM Divides the bandwidth of a single link into separate time slots. It transmits *two or more channels* over the same link by allocating different time slots of the transmission for each channel.
 
>> This creates an issue though, that data is being alloted as A > B > C > D, even if there is nothing is B and C, time is alloted for them. STDM Fixes this issue.

![TDM](img/chapter3/TDM?raw=true)


>__Statistical Time Division Multiplexing (STDM)__
>>  STDM uses a variable time slot length which allows channels to compete for unused time slots. 


![STDM](img/chapter3/STDM?raw=true)

---
## Definitions 

__Leased-Line Connection (LAN-to-WAN):__ Lines are leased from a carrier (usually a telephone company)  and are dedicated for use by the company

